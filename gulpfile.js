var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var purify = require('gulp-purifycss');
var browserSync = require('browser-sync').create();
gulp.task('ready_all', function() {
  gulp.src('app/js/*.js')
      .pipe(concat('scripts.js'))
      .pipe(uglify())
      .pipe(gulp.dest('app/js'));
  gulp.src('app/css/*.css')
    .pipe(concat('styles.css'))
    .pipe(purify(['app/js/semantic.js', 'app/index.html']))
    .pipe(minify())
    .pipe(gulp.dest('app/css'));
});

gulp.task('ready_css', function() {
  gulp.src('app/css/*.css')
    .pipe(concat('styles.css'))
    .pipe(purify(['app/js/semantic.js', 'app/index.html']))
    .pipe(minify())
    .pipe(gulp.dest('app/css'));
});

gulp.task('ready_js', function() {
  gulp.src('app/js/*.js')
      .pipe(concat('scripts.js'))
      .pipe(gulp.dest('app/js'));
});
